import pymysql as pms
import datetime

DB_FILENAME = 'ebay.db'


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """

    conn = pms.connect(host='127.0.0.1', user='root', password='password', database='ebay')
    return conn


def create_product(cur, product):
    """
    Create a new product into the products table
    :param conn:
    :param product:
    :return: project id
    """
    sql = '''
            INSERT INTO products
            (url, title, prd_condition, price, sold_for, shipping_cost, return_policy,
            seller_url, seller_name, seller_feedback, seller_recommenders, search_term_id)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) 
            '''
    cur.execute(sql, product)
    return cur.lastrowid


def create_search_term(cur, search_term):
    """
    Create a new product into the products table
    :param cur:
    :param search_term:
    :return: project id
    """
    sql = '''
            INSERT INTO search_terms
            (scraped_at, term)
            VALUES (%s, %s)
            '''
    cur.execute(sql, search_term)
    return cur.lastrowid


def update_products_db(term, link_attributes):
    with create_connection(DB_FILENAME) as conn:
        search_term = (datetime.datetime.now(), term)
        search_term_id = create_search_term(conn, search_term)

        for key, product in link_attributes.items():
            prod = (product['url'],
                    product['product name'],
                    product['product_condition'],
                    product['product price'],
                    product['sold amount'],
                    product['shipping cost'],
                    product['return policy'],
                    product['seller_url'],
                    product['seller name'],
                    product['seller feedback'],
                    product['seller recommenders'],
                    search_term_id)

            try:
                create_product(conn, prod)
            except Exception as e:
                print(f"Failed to create product {product['product name']}: {e}")


def read_database():
    with pms.connect(host='127.0.0.1', user='root', password='password', database='ebay') as cur:
        query = ('''
                    SELECT *
                    FROM products
                    ''')
        result = cur.execute(query)
        print(result)
