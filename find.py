from ebaysdk.finding import Connection as Finding
from extract_data_from_api import get_product_data

# api = Finding(domain='svcs.sandbox.ebay.com', appid="DoronBlu-sababoka-SBX-2b44fe1f6-4334a2c7", config_file=None)

api = Finding(https=True, debug=True, domain='svcs.sandbox.ebay.com', appid="DoronBlu-sababoka-SBX-2b44fe1f6-4334a2c7",
              config_file=None)

request = {
    'keywords': 'nike shoes',
    'itemFilter': [
        {'name': 'condition', 'value': 'new'}
    ],
    'outputSelector': [
        'StoreInfo', 'SellerInfo'
    ],
    'paginationInput': {
        'entriesPerPage': 1,
        'pageNumber': 1
    },
    'sortOrder': 'PricePlusShippingLowest'
}

response = api.execute('findItemsByKeywords', request)

# response = api.execute('findItemsAdvanced', {'keywords': 'Python'})


# print(response.dict)

products_list = response.dict()['searchResult']['item']
for item in products_list:
    print(get_product_data(item))

#
