from collections import defaultdict
from main import chrome_header, cook_some_soup
import re


def get_product_info_dict(url):
    headers = chrome_header()
    soup = cook_some_soup(headers, url)

    product_dict = defaultdict(lambda: None)

    product_dict['url'] = url

    item = soup.find('h1', id='itemTitle').contents[1]  # <h1 class="it-ttl" id="itemTitle" itemprop="name">
    product_dict['product name'] = item

    condition = soup.find('div', id='vi-itm-cond').contents[
        0]  # <div class="u-flL condText" id="vi-itm-cond" itemprop="itemCondition">
    product_dict['product_condition'] = condition

    item = soup.find(class_="mfe-reco-ct")
    if item:
        price_ils = item.get("data-price")
    else:
        price_ils = next(soup.find(id="convbinPrice").stripped_strings).split()[1]
    product_dict['product price'] = price_ils

    found = soup.find('span', class_='vi-qtyS-hot-red')
    if found:
        sold_amount = found.find('a').text.split()[0].replace(',', '')
        # print(sold_amount)
        product_dict['sold amount'] = sold_amount

    shipping_cost = soup.find(class_="mfe-reco-ct").get("data-shipping-cost")
    product_dict['shipping cost'] = shipping_cost

    return_policy = soup.find('span', id='vi-ret-accrd-txt').text
    # print(return_policy)
    product_dict['return policy'] = return_policy

    seller_name = soup.find('span', class_='mbg-nw').text
    # print(seller_name)
    product_dict['seller name'] = seller_name

    seller_pos_fb = soup.find('div', id='si-fb').text
    seller_pos_fb_num = re.search(r'((.*\d){2})', seller_pos_fb).group(1)
    product_dict['seller feedback'] = seller_pos_fb_num

    seller_num_of_recommneders = soup.find('span', class_='mbg-l').find('a').text
    product_dict['seller recommenders'] = seller_num_of_recommneders

    a = soup.find('div', class_='mbg vi-VR-margBtm3').find('a', href=True)
    seller_url = a['href']
    product_dict['seller_url'] = seller_url

    return product_dict
