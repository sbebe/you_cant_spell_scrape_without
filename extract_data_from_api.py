def get_product_data(item):
    product_dict = dict()

    product_dict['product name'] = item['title']
    product_dict['product_condition'] = item['condition']['conditionDisplayName']
    product_dict['product price'] = item['sellingStatus']['currentPrice']['value']
    # product_dict['sold amount'] = sold_amount
    product_dict['shipping cost'] = item['shippingInfo']['shippingServiceCost']['value']
    product_dict['return policy'] = item['returnsAccepted']
    product_dict['seller name'] = item['sellerInfo']['sellerUserName']
    product_dict['seller feedback'] = item['sellerInfo']['feedbackScore']
    # product_dict['seller recommenders'] = seller_num_of_recommneders
    # product_dict['seller_url'] = seller_url

    return product_dict#