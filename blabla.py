from ebaysdk import finding, tag, nodeText

f = finding()
f.execute('findItemsAdvanced', tag('keywords', 'shoes'))

dom    = f.response_dom()
mydict = f.response_dict()

# shortcut to response data
print(f.v('itemSearchURL'))

# process the response via DOM
items = dom.getElementsByTagName('item')

for item in items:
  print(nodeText(item.getElementsByTagName('title')[0]))