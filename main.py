"""
Author: Roy Sabato
"""

import argparse
from random import randrange

import pymysql as pms

import requests as rq
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import product_scrape
from time import sleep
import json
import sql_update

SEARCH = "search"
DB_CMD = 'db'
INIT_DB = '--init'
DROP_DB = '--drop'
MAX_LINKS = '--max_links'
HTML_PARSER = "html.parser"
EBAY_SEARCH_QUERY = "sch/i.html?_nkw="
EBAY_URL = "https://www.ebay.com"
DB_FILENAME = "ebay.db"


def construct_search_query(term):
    return f"{EBAY_URL}/{EBAY_SEARCH_QUERY}{term}"


def chrome_header():
    ua = UserAgent()
    chrome = ua.data_browsers['chrome'][0]
    headers = {'User-Agent': chrome}
    return headers


def cook_some_soup(headers, url):
    page = rq.get(url, headers=headers)
    return BeautifulSoup(page.content, HTML_PARSER)


def print_link_attributes(link_attributes):
    print("Attributes for links found:")
    for link, attributes in link_attributes.items():
        print(link, "=>")
        print(json.dumps(attributes, sort_keys=True, indent=2))
        print()


def parse_args():
    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers(title="Commands")
    sub_parsers.required = True

    parser_search = sub_parsers.add_parser(SEARCH)
    parser_search.set_defaults(action=SEARCH)
    parser_search.add_argument(SEARCH)
    parser_search.add_argument(MAX_LINKS, required=False, default=-1)

    parser_db = sub_parsers.add_parser(DB_CMD)
    parser_db.set_defaults(action=DB_CMD)
    db_grp = parser_db.add_mutually_exclusive_group()
    db_grp.required = True
    db_grp.add_argument(INIT_DB, default=False, action='store_true')
    db_grp.add_argument(DROP_DB, default=False, action='store_true')

    args = parser.parse_args()
    return args


def extract_link_attributes(result_links):
    print(f"Extracting {len(result_links)} links with [1,10] seconds sleep between each, to avoid blocking.")
    link_attributes = {}
    for i, link in enumerate(result_links):
        print(f"Getting {i + 1}/{len(result_links)}...")
        try:
            link_attributes[link] = product_scrape.get_product_info_dict(link)
        except Exception as e:
            print(f"Encountered error: {e}")
        sleep(randrange(10) + 1)
    return link_attributes


def get_result_links(term, max_links):
    url = construct_search_query(term)
    headers = chrome_header()
    soup = cook_some_soup(headers, url)
    elements = soup.select("a.s-item__link")
    if max_links == -1:
        max_links = len(elements)
    return [elm['href'] for elm in elements[:max_links]]


def init_db():
    with pms.connect(host='127.0.0.1', user='root', password='password') as cur:
        cur.execute('CREATE DATABASE IF NOT EXISTS ebay')

    with pms.connect(host='127.0.0.1', user='root', password='password', database='ebay') as cur:
        cur.execute('''
        CREATE TABLE IF NOT EXISTS search_terms
        (
          id INT NOT NULL AUTO_INCREMENT,
          scraped_at DATETIME NOT NULL,
          term VARCHAR(100) NOT NULL,
          PRIMARY KEY (id)
        );''')
        cur.execute('''
        CREATE TABLE IF NOT EXISTS products
        (
          id INT NOT NULL AUTO_INCREMENT,
          url VARCHAR(2000) NOT NULL,
          title VARCHAR(100) NOT NULL,
          prd_condition VARCHAR(100),
          price FLOAT NOT NULL,
          sold_for FLOAT,
          shipping_cost FLOAT,
          return_policy VARCHAR(100),
          seller_url VARCHAR(100) NOT NULL,
          seller_name VARCHAR(100),
          seller_feedback FLOAT NOT NULL,
          seller_recommenders INT NOT NULL,
          search_term_id INT NOT NULL,
          PRIMARY KEY (id),
          FOREIGN KEY (search_term_id) REFERENCES search_terms(id)
        );''')
    print("Database initialized successfully!")


def drop_db():
    with pms.connect(host='127.0.0.1', user='root', password='password') as cur:
        cur.execute('DROP DATABASE IF EXISTS ebay')
    print("Database dropped successfully!")


def main():
    """Parse search term from program arguments."""
    args = parse_args()
    if args.action == SEARCH:
        term = args.search
        max_links = int(args.max_links)
        print(f"Extracting results from search term <{term}>...")
        result_links = get_result_links(term, max_links)
        print(f"Found {len(result_links)} results, extracting attributes from each:")
        link_attributes = extract_link_attributes(result_links)
        # print_link_attributes(link_attributes)
        sql_update.update_products_db(term, link_attributes)
        # sql_update.read_database()
    elif args.action == DB_CMD:
        if args.init:
            try:
                init_db()
            except Exception as e:
                print("Encountered error initializing database:")
                print(e)
        elif args.drop:
            try:
                drop_db()
            except Exception as e:
                print("Encountered error dropping database:")
                print(e)


if __name__ == "__main__":
    main()
